class Todo {
  constructor(text, done = false) {
    this.text = text;
    this.done = done;
    this.liElement = '';
  }

  toggle() {
    // Màj le modèle
    this.done = !this.done;
    // Màj le HTML
    this.liElement.classList.toggle('done');
  }

  getHtmlElement() {
    const li = document.createElement('li');
    li.className = 'list-group-item' + (this.done ? ' done' : '');
    li.addEventListener('click', () => this.toggle());
    const textNode = document.createTextNode(this.text);
    const img = document.createElement('img');
    img.src = 'images/icon-delete.png';
    img.addEventListener('click', (event) => {
      // Ne déclenche pas de clic sur les balises parentes
      event.stopPropagation();
      if (confirm('Etes-vous sûr(e) ?')) {
        // Supprime le todo en cours du modèle
        todoList.deleteTodo(this);
        // Rafraîchit le HTML
        todoList.displayTodos();
      }
    });
    li.appendChild(img);
    li.appendChild(textNode);
    this.liElement = li;  // sauvegarde une référence à l'élément HTML

    return li;
  }

  toJson() {
    return {
      text: this.text,
      done: this.done,
    };
  }
}

class TodoList {

  constructor(title, ...todos) {
    this.title = title;
    this.todos = [...todos];
    this.currentStatus = 'ALL';
    this.setupForm();
    this.setupFilters();
  }

  addTodo(todo) {
    this.todos = this.todos.concat(todo);
    this.saveToStorage();
  }

  deleteTodo(todo) {
    this.todos = this.todos.filter(td => td.text !== todo.text);
    this.saveToStorage();
  }

  displayTodos() {
    const ul = document.getElementById('todos');
    ul.innerHTML = '';  // Repart toujours d'une liste vide.
    this.todos
      .filter(td => {
        if (this.currentStatus === 'ALL') {
          return true;
        } else if (this.currentStatus === 'DONE') {
          return td.done;
        } else if (this.currentStatus === 'NOT_DONE') {
          return !td.done;
        }
      })
      .forEach(td => ul.appendChild(td.getHtmlElement()));
  }

  // Ecoute le submit sur le form
  setupForm() {
    const form = document.getElementsByTagName('form')[0];
    form.addEventListener('submit', (event) => {
      event.preventDefault();
      const input = form.elements[0];
      const trimmedValue = input.value.trim();
      if (trimmedValue === '') {
        return alert('Vous devez saisir du texte.');
      }
      const alreadyExists = this.todos.some(td => td.text.toLowerCase() === trimmedValue.toLowerCase());
      if (alreadyExists) {
        return alert('Ce todo existe déjà.');
      }
      const todo = new Todo(input.value);
      this.addTodo(todo);
      this.displayTodos();
      input.value = '';
    });
  }

  // Ecoute les clics sur les filtres
  setupFilters() {
    const links = document.querySelectorAll('.nav-link');
    links.forEach(link => {
      link.addEventListener('click', event => {
        // Annule le rafraîchissement de page
        event.preventDefault();
        // Retire la classe active sur l'item active
        document.querySelector('.nav-link.active').classList.remove('active');
        // Ajoute la classe active sur le lien cliqué
        link.classList.add('active');
        // Actualise le statut courant
        this.currentStatus = link.getAttribute('data-status');
        // Rafraîchit la liste en la filtrant
        this.displayTodos();
      })
    });
  }

  saveToStorage() {
    const listAsString = JSON.stringify(this.toJson());
    window.localStorage.setItem('todos', listAsString);
  }

  toJson() {
    return {
      title: this.title,
      todos: this.todos.map(td => td.toJson())
    };
  }

  // Create a new list from JSON data
  static fromJson(jsonList) {
    return new TodoList(jsonList.title, ...jsonList.todos.map(todoJson => new Todo(todoJson.text, todoJson.done)));
  }
}

let todoList;

// Test si on a un localStorage
const storage = window.localStorage.getItem('todos');
if (storage) {
  const todoListJson = JSON.parse(storage);
  todoList = TodoList.fromJson(todoListJson);
} else {
  todoList = new TodoList('Ma première liste');
}

todoList.displayTodos();

//

// = new TodoList(
//   'Ma première liste',
//   new Todo('Faire les courses'),
//   new Todo('Réviser JavaScript'),
//   new Todo('Me coucher plus tôt'),
// );
