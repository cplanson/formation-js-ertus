define(["require", "exports", "./todoItem"], function (require, exports, todoItem_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Status;
    (function (Status) {
        Status["ALL"] = "ALL";
        Status["DONE"] = "DONE";
        Status["NOT_DONE"] = "NOT_DONE";
    })(Status || (Status = {}));
    class TodoList {
        constructor(title, ...todos) {
            this.currentStatus = Status.ALL;
            this.title = title;
            this.todos = [...todos];
            this.setupForm();
            this.setupFilters();
        }
        addTodo(todo) {
            this.todos = this.todos.concat(todo);
            this.saveToStorage();
        }
        deleteTodo(todo) {
            this.todos = this.todos.filter(td => td.text !== todo.text);
            this.saveToStorage();
        }
        displayTodos() {
            const ul = document.getElementById('todos');
            ul.innerHTML = ''; // Repart toujours d'une liste vide.
            this.todos
                .filter(td => {
                if (this.currentStatus === Status.ALL) {
                    return true;
                }
                else if (this.currentStatus === Status.DONE) {
                    return td.done;
                }
                else if (this.currentStatus === Status.NOT_DONE) {
                    return !td.done;
                }
            })
                .forEach(td => ul.appendChild(td.getHtmlElement()));
            ul.addEventListener('todoDelete', (event) => {
                const todo = event.detail.todo;
                this.deleteTodo(todo);
                this.displayTodos();
            });
            ul.addEventListener('saveTodo', event => {
                this.saveToStorage();
            });
        }
        // Ecoute le submit sur le form
        setupForm() {
            const form = document.getElementsByTagName('form')[0];
            form.addEventListener('submit', (event) => {
                event.preventDefault();
                const input = form.elements[0];
                const trimmedValue = input.value.trim();
                if (trimmedValue === '') {
                    return alert('Vous devez saisir du texte.');
                }
                const alreadyExists = this.todos.some(td => td.text.toLowerCase() === trimmedValue.toLowerCase());
                if (alreadyExists) {
                    return alert('Ce todo existe déjà.');
                }
                const todo = new todoItem_1.Todo(input.value);
                this.addTodo(todo);
                this.displayTodos();
                input.value = '';
            });
        }
        // Ecoute les clics sur les filtres
        setupFilters() {
            const links = document.querySelectorAll('.nav-link');
            links.forEach(link => {
                link.addEventListener('click', event => {
                    // Annule le rafraîchissement de page
                    event.preventDefault();
                    // Retire la classe active sur l'item active
                    document.querySelector('.nav-link.active').classList.remove('active');
                    // Ajoute la classe active sur le lien cliqué
                    link.classList.add('active');
                    // Actualise le statut courant
                    this.currentStatus = link.getAttribute('data-status');
                    // Rafraîchit la liste en la filtrant
                    this.displayTodos();
                });
            });
        }
        saveToStorage() {
            const listAsString = JSON.stringify(this.toJson());
            window.localStorage.setItem('todos', listAsString);
        }
        toJson() {
            return {
                title: this.title,
                todos: this.todos.map(td => td.toJson())
            };
        }
        // Create a new list from JSON data
        static fromJson(jsonList) {
            return new TodoList(jsonList.title, ...jsonList.todos.map(todoJson => new todoItem_1.Todo(todoJson.text, todoJson.done)));
        }
    }
    exports.TodoList = TodoList;
});
