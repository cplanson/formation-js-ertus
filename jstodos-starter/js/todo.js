define(["require", "exports", "./todoList"], function (require, exports, todoList_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function displayCurrentPage() {
        const currentPage = location.hash.substr(1);
        document.querySelectorAll(".page").forEach(page => {
            console.log("ma page " + page.id);
            if (page.id === currentPage) {
                page.classList.remove('d-none');
            }
            else {
                page.classList.add('d-none');
            }
        });
    }
    window.addEventListener('hashchange', displayCurrentPage);
    if (location.hash) {
        displayCurrentPage();
    }
    else {
        location.hash = 'detail';
    }
    document.getElementById("myButton").addEventListener("click", () => showModal('coucou', 'test toto'));
    function showModal(title, text) {
        const modale = document.getElementById('myModal');
        document.querySelector('#myModal .modal-title').textContent = title;
        document.querySelector('#myModal .modal-body p').textContent = text;
        modale.classList.add('d-block');
        return new Promise((resolve, reject) => {
            const closeButtons = document.querySelectorAll('#myModal .close, #myModal .btn-secondary');
            closeButtons.forEach(el => el.addEventListener('click', () => {
                modale.classList.remove('d-block');
                if (reject !== undefined) {
                    reject();
                }
            }));
            document.querySelector('#myModal .btn-primary').addEventListener('click', () => {
                modale.classList.remove('d-block');
                resolve();
            });
        });
    }
    //Load todolist from db
    function loadTodoList(todoListId) {
        return fetch('http://localhost:3000/todolists/' + todoListId).then(resp => resp.json());
    }
    loadTodoList(1).then(json => {
        todoList_1.TodoList.fromJson(json).displayTodos();
    });
});
