define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class Todo {
        constructor(text, done = false) {
            this.text = text;
            this.done = done;
        }
        toggle() {
            // Màj le modèle
            this.done = !this.done;
            // Màj le HTML
            this.liElement.classList.toggle('done');
            const customEvent = new CustomEvent('saveTodo', { bubbles: true, detail: { todo: this } });
            this.liElement.dispatchEvent(customEvent);
        }
        getHtmlElement() {
            const li = document.createElement('li');
            li.className = 'list-group-item' + (this.done ? ' done' : '');
            li.addEventListener('click', () => this.toggle());
            const textNode = document.createTextNode(this.text);
            const img = document.createElement('img');
            img.src = 'images/icon-delete.png';
            img.addEventListener('click', (event) => {
                // Ne déclenche pas de clic sur les balises parentes
                event.stopPropagation();
                event.preventDefault();
                showModal("Vraiment ?", 'Etes-vous sûr(e) ?').then(() => {
                    // Supprime le todo en cours du modèle
                    const customEvent = new CustomEvent('todoDelete', { bubbles: true, detail: { todo: this } });
                    img.dispatchEvent(customEvent);
                });
            });
            li.appendChild(img);
            li.appendChild(textNode);
            this.liElement = li; // sauvegarde une référence à l'élément HTML
            return li;
        }
        toJson() {
            return {
                text: this.text,
                done: this.done,
            };
        }
    }
    exports.Todo = Todo;
});
