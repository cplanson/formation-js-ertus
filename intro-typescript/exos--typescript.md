# Formation TypeScript 2.x - Exercices



## EXO 1 : Installer l'environnement de développement


### Logiciels

Les logiciels suivants doivent être installés sur votre machine :

- [Node.js](https://nodejs.org/) et npm (au minimum node v6.9.x et npm 3.x).
- IDE qui supporte TypeScript. [Visual Studio Code](https://code.visualstudio.com) conseillé.
- Navigateur web moderne, par exemple Google Chrome.


### TypeScript

Installer le compilateur TypeScript **globalement** sur votre machine :

```bash
npm install -g typescript
```

### Fichiers de formation

Téléchargez les fichiers suivants sur le présent dépôt Github :

- Le support de cours en PDF.
- Les fichiers pour faire les exercices.



## EXO 2 : Bootstrapper l'application VendingMachine

⚠️ Utilisez les fichiers (vides) ont déjà été créés pour vous dans le répertoire `ts` de l'application :

- `bootstrapper.ts`
- `vending-machine.ts`

Dans sa version la plus simple, une "application" est une simple classe (ou une fonction) qui contient tout le code de l'application :

```typescript
class VendingMachine {
  // ...
}
```

QUESTION COLLECTIVE : Pourquoi est-ce une bonne idée de séparer le code de l'application et le code qui démarre ("bootstrape") l'application ?



## EXO 3 : Mettre des pièces dans le distributeur

Dans le fichier `ts/coins.ts`, créez les classes permettant de modéliser les pièces à insérer dans le distributeur :

Chaque classe devra posséder une propriété et une méthode :

- Propriété `value` (valeur de la pièce)
- Méthode `getImageUrl()` (renvoie le chemin physique de l'image de la pièce)

Liste des classes à créer :

- `Quarter` - Valeur 0.25 - Image "img/Quarter.png"
- `Dime` - Valeur 0.10 - Image "img/Dime.png"
- `Half` - Valeur 0.50 - Image "img/Half.png"
- `Dollar` - Valeur 1 - Image "img/Dollar.jpg"

Ensuite, implémentez dans la classe `VendingMachine` :

- Propriété `total` qui contient le montant total des pièces insérées dans le distributeur.
- Méthode `acceptCoin(coin)` qui accepte en paramètre n'importe quelle pièce, et qui ajoute sa valeur au total du distributeur.

Enfin, faites en sorte qu'un clic sur le bouton "Insérer pièce" déclenche la méthode `acceptCoin()`, qui rafraîchit le total disponible (`document.getElementById('total').textContent = "..."`).


## EXO 4 : Afficher les différentes pièces

Dans la classe `VendingMachine`, créez une propriété `acceptedCoins` qui contient la liste de toutes les pièces acceptées par la machine.

Dans le HTML, affichez toutes ces pièces à la place du bouton "Insérer pièce" en utilisant une boucle sur la balise `<img>` :

```html
<div>
  <img src="IMAGE_DE_LA_PIECE"> <!-- faire une boucle ici -->
</div>
```

Ensuite, faites en sorte qu'on puisse **mettre des pièces dans le distributeur** :

- Quand on clique sur une pièce, elle doit être "ajoutée" au distributeur (méthode `acceptCoin()`).
- Le `total` du distributeur doit être recalculé et rafraîchi dans le HTML.



## EXO 5 : Créer les modèles "produit", "catégorie", et "emplacement"

### Catégories

Dans le fichier `ts/categories.ts`, ajoutez le code suivant :

```typescript
class SodaCategory {
  name = 'Soda';
  getImageUrl() {
    return 'img/SodaCan.png';
  }
}

class NutsCategory {
  name = 'Nuts';
  getImageUrl () {
    return 'img/Nuts.png';
  }
}

class ChipsCategory {
  name = 'Potato chips';
  getImageUrl () {
    return 'img/Chips.png';
  }
}

class CandyCategory {
  name = 'Candy';
  getImageUrl () {
    return 'img/Candy.png';
  }
}

class CandyBarCategory {
  name = 'Candy bar';
  getImageUrl () {
    return 'img/CandyBar.png';
  }
}
```

### Produits

Dans le fichier `ts/products.ts`, ajoutez le code suivant :

```typescript
class CocaCola {
  name = 'Coca-Cola';
  price = 2.3;
  category = new SodaCategory();
}

class Fanta {
  name = 'Fanta';
  price = 2;
  category = new SodaCategory();
}

class Sprite {
  name = 'Sprite';
  price = 1.80;
  category = new SodaCategory();
}

class Peanuts {
  name = 'Peanuts';
  price = 1.50;
  category = new NutsCategory();
}

class Cashews {
  name = 'Cashews';
  price = 2.80;
  category = new NutsCategory();
}

class Plain {
  name = 'Plain';
  price = 2;
  category = new ChipsCategory();
}

class Cheddar {
  name = 'Cheddar';
  price = 2;
  category = new ChipsCategory();
}

class Mints {
  name = 'Mints';
  price = 1.30;
  category = new CandyCategory();
}

class Gummies {
  name = 'Gummies';
  price = 1.90;
  category = new CandyCategory();
}

class Hersey {
  name = 'Hersey\'s';
  price = 1.30;
  category = new CandyBarCategory();
}

class MilkyWay {
  name = 'Milky Way';
  price = 1.80;
  category = new CandyBarCategory();
}
```

### Emplacements ("Cell")

Pour info : un emplacement, ou "cell", permet de définir quel produit est vendu dans quel emplacement du distributeur, et en quelle quantité.

Créez le modèle `Cell` (à mettre dans le fichier `ts/vending-machine.ts`) :

- Propriétés : `product` (produit contenu dans l'emplacement), `stock` (nombre d'unités disponibles) et `sold` (booléen indiquant si le produit est vendu ou non).
- Méthode : aucune.


## EXO 6 : Remplir le distributeur de produits


### 1. Initialiser la VendingMachine avec des produits

- Ajoutez une propriété `cells` sur la classe `VendingMachine` qui contient tous les emplacements ("Cell") du distributeur.
- La propriété `cells` doit être initialisée **à la création de l'application** en assignant à chaque emplacement ("Cell") un produit  et un stock. Le produit sera récupéré aléatoirement (voir code ci-dessous) et le stock sera de "3" par défaut.
- Le nombre de cells disponibles dans le distributeur doit être **paramétrable à la création de l'application**. Les valeurs possibles sont 6, 9 ou 12 (indice : utilisez un enum `VendingMachineSize`).

Code pour récupérer un produit aléatoirement :

```typescript
function getProduct() {
  const random = Math.floor(Math.random() * 11);
  switch (random) {
    case 0: return new CocaCola();
    case 1: return new Fanta();
    case 2: return new Sprite();
    case 3: return new Peanuts();
    case 4: return new Cashews();
    case 5: return new Plain();
    case 6: return new Cheddar();
    case 7: return new Mints();
    case 8: return new Gummies();
    case 9: return new Hersey();
    case 10: return new MilkyWay();
  }
}
```

### 2. Afficher les produits de la VendingMachine dans le HTML

Prenez le HTML suivant comme modèle.

Le bloc de HTML à répéter pour chaque produit est `<div class="col-md-4 cell">...</div>`.

```html
<div class="col-md-8 machine">
  <div class="col-md-4 cell">
    <div class="col-md-6 image">
      <img src="__PRODUCT_IMAGE__" alt="__TEXTE_ALT__">
    </div>
    <div class="col-md-6 productInfo">
      <div>__PRODUCT_NAME</div>
      <h4>__STOCK__</h4>
      <h3>__PRODUCT_PRICE__ €</h3>
    </div>
  </div>
</div>
```


## EXO 7 : Sélectionner un produit et payer

Dans `VendingMachine`, ajoutez :

- Une propriété `selectedCell` représentant l'emplacement actuellement sélectionné.
- Une méthode `selectCell(cell)`, qui assigne la cell passée en paramètre à la propriété `this.selectedCell`.
- Une méthode `pay()` qui permet d'acheter le produit correspondant à l'emplacement sélectionné. Cette méthode doit :
  - Ne rien faire si le produit est épuisé.
  - Déduire le prix du produit sélectionné du montant inséré dans la machine.
  - Retirer une unité du stock du produit sélectionné.
  - Déclencher une animation CSS, en ajoutant la classe "sold" déjà définie à la balise `<img>` du produit cliqué.

Dans le HTML, reliez les éléments d'interface suivants au code JavaScript :

- Lorsqu'une cellule est cliquée, appelez la méthode `selectCell()` en trouvant un moyen de lui passer la cellule cliquée en paramètre.
- Ajoutez un bouton "Payer" dans le HTML et appelez la méthode `pay()` lorsqu'il est cliqué.
- Trouver un moyen de désactiver le bouton "Payer" si le montant inséré dans le distributeur est inférieur au prix du produit sélectionné. Indice : Désactiver un bouton en HTML : `<button type="button" disabled="true">.../<button>`

----

⚠️ Selon la manière dont vous structurez votre code, il risque de planter s'il n'y a aucun produit sélectionné initialement. Pour éviter ce problème, on pourrait créer un produit initial "bidon", par exemple :

```typescript
class InitialProduct {
  name = 'Pas de produit';
  price = 0;
}
```



## EXO 8 : Utiliser l'héritage pour les pièces et les catégories

Faites en sorte que chaque pièce et chaque catégorie hérite d'une classe abstraite parente, respectivement `Coin` et `ProductCategory`.

⚠️ N'oubliez pas de donner les bons types TypeScript à chaque propriété !



## EXO 9 : Utiliser une interface pour les produits

Créez une interface `Product` qui impose à chaque produit d'avoir une propriété `name`, une propriété `price` et une propriété `category`.



## EXO 10 : Refactoriser l'appli pour utiliser les modules

Utilisez le pattern `import` / `export` dans votre code :

- Paramétrez la propriété `compilerOptions.module` de `tsconfig.json` sur "amd".
- Refactorisez tout votre code pour utiliser `import` et `export` **à la place** des directives `<reference path>`

Installez require.js pour résoudre les imports :

- Chargez le fichier `lib/require.js` dans le fichier HTML.
- Amorcez le chargement initial du projet avec require, en exécutant la ligne de code suivante : `require(["js/bootstrap"]);`

